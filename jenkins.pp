package { 'java-1.8.0-openjdk':
  ensure => present,
}

exec { 'executando o jogovelha':
  command => '/usr/bin/java -jar jogovelha-0.0.1-SNAPSHOT.jar &',
  path => '/home/grupo5',
}
